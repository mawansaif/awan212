@extends('layouts.app')
@section('content')
<div class="container">
	<a href="{{ url('post') }}" class="btn btn-success">Back</a>
	<hr>

	<form class="m-2" method="POST" action="/post/update/{{$data->id}}" >
		<h1>Edit POST</h1>
		<hr>
		@csrf
		
		<input type="text" name="title" class="form-control mt-2" placeholder="title" value="{{$data->title}}">
		<input name="body" class="form-control mt-2" placeholder="body" value="{{$data->description}}">
		<textarea name="content" class="form-control">{{$data->detail->content}}
		</textarea>
		<input type="author" name="author" class="form-control mt-2" placeholder="author" value="{{$data->author}}">
		<select class="form-control mt-2" name="category">
			@foreach($categories as $category)
			@if($data->category->id === $category->id)
				<option value="{{$category->id}}" selected>
					{{$category->category}}
				</option>
			@else
				<option value="{{$category->id}}">{{$category->category}}</option>
			@endif	
			@endforeach
		</select>
		<button class="btn btn-success btn-lg mt-2">Update</button>
	</form>

	
</div>
@endsection
