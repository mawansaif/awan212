<?php

namespace App\Http\Controllers;

use App\Post_detail;
use App\Post;
use Illuminate\Http\Request;

class PostDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($post)
    {
        //
        //dd($post);
        $data = Post::with('detail','category')->find($post);
        return view('post.detail',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post_detail  $post_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Post_detail $post_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post_detail  $post_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(Post_detail $post_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post_detail  $post_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post_detail $post_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post_detail  $post_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post_detail $post_detail)
    {
        //
    }
}
