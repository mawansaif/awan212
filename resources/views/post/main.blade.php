@extends('layouts.app')
@section('content')
<div class="d-flex p-3 flex-nowrap justify-content-end bg-dark">
	<h1 class="text-light mr-auto">My POST</h1>
	<a href="{{ url('create') }}" class="btn btn-success btn-lg">Add POST</a>
</div>

<div class="container">
	<br>
	<br>
	@if(session('message'))

		<div class="mt-2 alert alert-success">
			{{session('message')}}
		</div>
	@endif



	<!-- display post detail -->
	<div class="mt-4 table-responsive">
		<table class="table table-bordered text-center rounded mt-4 table-striped">
			<thead>
				<tr>
				  <th scope="col">#</th>
				  <th scope="col">title</th>
				  <th scope="col">Body</th>
				  <th scope="col">Author</th>
				  <th scope="col">Category</th>
				  <th scope="col">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data as $key => $value)
				<tr>
				  <th scope="row">{{$key+1}}</th>
				  <td>{{$value->title}}</td>
				  <td>{{$value->description}}</td>
				  <td>{{$value->author}}</td>
				  <td>{{$value->category->category}}</td>
				  <td>
				  	<a href="{{ url('post/detail') }}/{{$value->id}}" class="btn btn-success">View</a>
				  	<a href="{{ url('post/edit') }}/{{$value->id}}" class="btn btn-success">Edit</a>
				  	<button class="btn btn-danger btn-delete" data-id="{{ $value->id }}">Remove</button>
				  </td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>




	<!-- delete post Modal -->
	<div class="modal fade" id="deletePostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-dark text-light">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Conformation</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				<p>Are you sure to delete this post?</p>
				<form method="POST" action="/post/delete">
					@csrf
					<input type="hidden" name="post_id" id="post_id">
					<button class="btn btn-danger">Yes, Sure!</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</form>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
@section('scripts')
<script type="text/javascript">
	
	$(document).ready(function(){
		$(".btn-delete").on('click',function(){
			var post_id = $(this).attr('data-id');
			$("#post_id").val(post_id);
			$("#deletePostModal").modal('show');
		});

	});
</script>
@endsection
