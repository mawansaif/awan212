<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Blog') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mystyle.css') }}" rel="stylesheet"> <!-- custom style sheet -->
</head>
<body>
    <div id="app">
        <!-- include header -->
        @include('partial.header')
        <!-- main content part -->
        <main class="">
            @yield('content')
        </main>
    </div>
</body>
<!-- include footer -->
@guest <!-- with some condition -->

@else
    @include('partial.footer')
    @yield('scripts')
@endguest

</html>
