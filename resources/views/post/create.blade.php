@extends('layouts.app')
@section('content')
<div class="d-flex flex-nowrap justify-content-start bg-dark p-3">
	<a href="{{ url('post') }}" class="btn btn-success btn-lg align-content-start">Back</a>
	<h1 class="align-content-center text-light ml-auto mr-5">Add New POST</h1>
</div>
<div class="container">
	
	<hr>
	<form class="m-2" method="POST" action="/post/save" >
		@csrf
		 @if ($errors->any())
		 	<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
				 {{$error}}
				@endforeach
			</div>
		@endif
		<label>Title:</label>
		<input type="text" name="title" class="form-control form-control-lg"  placeholder="title">
		<label>Description:</label>
		<input type="text" name="body" class="form-control form-control-lg" required placeholder="description">
		<label>Body:</label>
		<textarea name="content" class="form-control form-control-lg" required placeholder="content"></textarea>
		<label>Author:</label>
		<input type="author" name="author" class="form-control form-control-lg" required placeholder="author">
		<label>Category:</label>
		<select class="form-control form-control-lg" name="category">
			@foreach($categories as $category)
				<option value="{{$category->id}}">{{$category->category}}</option>	
			@endforeach
		</select>
		<button class="btn btn-success btn-lg mt-4 mb-4 w-25">Save</button>
	</form>

	
</div>
@endsection
