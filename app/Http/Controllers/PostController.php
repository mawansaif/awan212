<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use App\Post_detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Post::with('Category')->get();
        return view('post.main',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = category::get();
        return view('post.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validator = Validator::make($request->all(), [
            'title' => 'required','string', 'max:100',
            'body' => 'required',
            'author' => 'required','max:20',
        ]);
        if($validator->fails()) {
            return redirect()->route('post/create')->withErrors($validator);
        }
        else
        {
            $data = new Post;
            $data->title = $request->title;
            $data->description = $request->body;
            $data->author = $request->author;
            $data->category_id = $request->category;
            $check = $data->save();
            if($check)
            {
                $data_detail = new Post_detail;
                $data_detail->content = $request->content;
                $data_detail->post_id = $data->id;
                $check = $data_detail->save();
                if($check){
                    return redirect()->route('post')->with('message','Post save successfully');
                } 
                else
                {
                    return redirect()->route('post')->with('message','Oops, Post not save');
                }
            }
            else
            {
             return redirect()->route('post')->with('message','Oops, there is error');
            }
           
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
         $data = $post;
         $categories = category::get();
         return view('post.edit',compact('data'))->with('categories',$categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
        //dd($post->id);
        $data = Post::where('id',$post->id)->update(['title'=>$request->title,'description'=> $request->body,'author' => $request->author,'category_id'=>$request->category]);
        $data = Post_detail::where('post_id',$post->id)->update(['content'=>$request->content]);
        return redirect()->route('post')->with('message','Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Post $post)
    {
        //
        $id = $request->post_id;
        $data = Post::find($id);
        $data->delete();

        return redirect()->route('post')->with('message','Post deleted successfully');

    }
}
