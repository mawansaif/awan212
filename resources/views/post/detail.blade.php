@extends('layouts.app')
@section('content')
<div class="container">
	<a href="{{ url('post') }}" class="btn btn-success">Back</a>

	<h1 class="text-center">Blog Detail</h1>
	<hr>
	<div class="w-50 m-auto border p-4 shadow-sm">
		<h2>{{$data->title}}   -- -- {{$data->author}}</h2>
		<hr>
		<span class="mt-2 mb-2 d-block h4">Blog Description</span>
		<p>{{$data->description}}</p>
		<span class="mt-2 mb-2 d-block h2">Blog content</span>
		<p class="text-justify">{{$data->detail->content}}</p>
		<span class="mt-2 mb-2 d-block h2">Blog category</span>
		<p>{{$data->category->category}}</p>
		<hr>
		<a href="{{ url('post/edit') }}/{{$data->id}}" class="btn btn-success">Edit</a>
		<button data-id="{{$data->id}}" class="btn btn-danger btn-delete float-right">remove</button>
	</div>


	


	<!-- delete Modal -->
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				<p class="h2">Are you sure to delete this post?</p>
				<form class="mt-4" method="POST" action="/post/delete">
					@csrf
					<input type="hidden" name="post_id" id="post_id">
					<button class="btn btn-danger">Yes, Sure!</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
				</form>
				</div>
			</div>
		</div>
	</div>
	
</div>
@endsection



@section('scripts')
<script type="text/javascript">
	
	$(document).ready(function(){
		$(".btn-delete").on('click',function(){
			var post_id = $(this).attr('data-id');
			$("#post_id").val(post_id);
			$("#deleteModal").modal('show');
		});

	});
</script>
@endsection