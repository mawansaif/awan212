@extends('layouts.app')
	
@section('content')	
        <div class="flex-center position-ref full-height">
     <!-- @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif -->
			
            <div class="content">
				<!-- display -->
				<h1 class="text-center bg-dark text-light p-3"> My Blogs </h1>
				<div class="container">
					@if (session('savemessage'))
						<div class="alert alert-success">
							{{ session('savemessage') }}
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							  </button>
						</div>
					@endif
					<div class="m-4">
					@if (session('updatemessage'))
						<div class="alert alert-success">
							{{ session('updatemessage') }}
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							  </button>
						</div>
					@endif
					
					@if (session('deletemessage'))
						<div class="alert alert-success">
							{{ session('deletemessage') }}
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							  </button>
						</div>
					@endif
						<table class="table table-bordered text-center">
						<button class="btn btn-success btn-add-blog float-right mb-2">Add Blog </button>
							<thead>
								<tr>
								<th scope="col">Title</th>
								<th scope="col">Description</th>
								<th scope="col">Author</th>
								<th> Action</th>
								</tr>
							</thead>
							<tbody>
							
								@foreach ($data as $info)
								<tr>
									<td>{{$info->title}}</td>
									<td>{{$info->description}}</td>
									<td>{{$info->author}}</td>
									<td>
									<button data-id="{{$info->id}}" data-title="{{$info->title}}" data-description="{{$info->description}}"  data-author="{{$info->author}}" class="btn btn-success btn-edit" >Edit</button>
									<button data-id="{{$info->id}}" class="btn btn-danger btn-delete" >Remove</button>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="mr-auto">
							{{ $data->links() }}
						</div>
					</div>
				</div>
				
				<!-- add blog Modal -->
				<div class="modal fade" id="addModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Add blog</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						<form method="POST" action="">
							@csrf
							<label>Title</label>
							<input name="title" id="title" class="form-control" />
							<label>Body</label>
							<Textarea name="body" id="body" class="form-control" /></Textarea>
							<label>Author</label>
							<input name="author" id="autor" class="form-control" />
							<button type="submit" class="btn btn-success mt-1">Save</button>
						</form>
					  </div>
					  <div class="modal-footer">
						
					  </div>
					</div>
				  </div>
				</div>
				
				<!-- edit Modal -->
				<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit Blog</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						<form method="POST" action="/update">
							@csrf
							<label>Title</label>
							<input name="title" id="edit_title" class="form-control" />
							<label>Body</label>
							<Textarea name="body" id="edit_body" class="form-control" /></Textarea>
							<label>Author</label>
							<input name="author" id="edit_author" class="form-control" />
							<input name="id" id="id" type="hidden" />
							<button type="submit" class="btn btn-success mt-1">Update</button>
							
						</form>
					  </div>
					  <div class="modal-footer">
						
						
					  </div>
					</div>
				  </div>
				</div>
				
				<!-- confirm delete -->
				<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Delete</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
					  <p>Are you Sure to Delete this blog?</p>
						<form method="POST" action="/delete">
							@csrf
							<input name="blogId" id="blog_id" type="hidden" >
							<button type="submit" class="btn btn-danger btn-yes">Yes Sure!</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Not</button>
						</form>
					  </div>
					  <div class="modal-footer">
						
						
					  </div>
					</div>
				  </div>
				</div>
				
            </div>
        </div>
@endsection


@section('scripts')

		<script>
		$(document).ready(function(){
			
			// show add blog modal
			$('.btn-add-blog').on('click',function(){
				$('#addModel').modal('show');
			});
			
			
			// edit blog
			$(".btn-edit").on('click',function(){
				
				$("#edit_title").val('');
				$("#edit_body").val('');
				$("#edit_author").val('');
				
				var id = $(this).attr("data-id");
				var title = $(this).attr("data-title");
				var body = $(this).attr("data-description");
				var author = $(this).attr("data-author");
				$("#edit_title").val(title);
				$("#edit_body").val(body);
				$("#edit_author").val(author);
				$("#id").val(id);
				$("#modal").modal('show');
			});
			$(".btn-delete").on('click',function(){
				$("#blog_id").val('');
				var id = $(this).attr("data-id");
				$("#blog_id").val(id);
				$("#deleteModal").modal('show');
				
			});
		});
		</script>

@endsection