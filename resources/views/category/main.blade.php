@extends('layouts.app')
@section('content')
<div class="row bg-dark p-3 mb-2 text-center">
	<div class="col-5">
		<a href="{{ url('post') }}" class="btn btn-success">Back</a>
	</div>
	<div class="çol-2 text-light">
		<h1 class="text-light text-center rounded">Categories</h1>
	</div>
	<div class="col-4">
		<button class="btn btn-success btn-add-category float-right">Add Category</button>
	</div>
</div>
<div class="container">
	@if(session('savemessage'))
		<div class="alert alert-success mt-2">
			{{session('savemessage')}}
		</div>
	@endif



	<!-- Modal to add category -->

	<div class="modal " id="addCategoryModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-lg ">
				<div class="modal-header">
					<h5 class="modal-title">Add Category</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="m-2" action="" method="POST" >
						@csrf
						<input type="text" name="category" class="form-control form-control-lg mt-2" placeholder="Category">
						<button class="btn btn-success btn-lg mt-4 w-25">Save</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	

	<!-- display categories -->
	<div class="'table-responsive">
		<table class="m-auto w-75 table-bordered text-center rounded table table-striped">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Category title</th>
		      <th scope="col">Ation</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($data as $value) 
		    <tr>
	      		<th scope="row">{{$value->id}}</th>
	      		<td>{{$value->category}}</td>
	      		<td>
	      			<button data-id="{{$value->id}}" data-value="{{ $value->category }}" class="btn btn-success btn-edit">Edit</button>
	      			<button data-id="{{$value->id}}" class="btn btn-danger btn-delete">Remove</button>
	      		</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	</div>



	<!-- Modal to edit category -->

	<div class="modal" id="editCategoryModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Category</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="m-2" action="/category/update" method="POST" >
						@csrf
						<input type="text" name="category" id="edit_category" class="form-control mt-2" placeholder="Category">
						<input type="hidden" name="category_id" id="category_id">
						<button class="btn btn-success btn-lg mt-2">Update</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	



	<!-- Modal to delete category -->

	<div class="modal" id="deleteCategoryModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Conformation</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure to delete this category?</p>
					<form method="POST" action="/category/delete">
						@csrf
						<input type="hidden" name="category_id" id="delete_category_id">
						<button class="btn btn-danger">Yes, Sure!</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</form>
				</div>
				<div class="modal-footer">
				
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		$(".btn-add-category").on('click',function(){

			$("#addCategoryModal").modal('show');
		});


		$(".btn-edit").on('click',function(){
			var category = $(this).attr('data-value');
			var category_id = $(this).attr('data-id');
			$("#edit_category").val(category);
			$("#category_id").val(category_id);
			$("#editCategoryModal").modal('show');
		});

		$(".btn-delete").on('click',function(){
			var category_id = $(this).attr('data-id');
			$("#delete_category_id").val(category_id);
			$("#deleteCategoryModal").modal('show');
		});
	});
</script>
@endsection