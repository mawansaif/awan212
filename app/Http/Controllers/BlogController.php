<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$data = Blog::orderBy('updated_at','desc')->paginate(5);
		return view('welcome',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		
		
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$save = new Blog;
		$save->title = $request->title;
		$save->description = $request->body;
		$save->author = $request->author;
		$save->save();
		
		//$data = new Blog;
		return redirect()->route('home')->with('savemessage', 'Blog saved successfully');
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
		$id  = $request->id;
		$data = Blog::where('id',$id)->update(['title' => $request->title,
												'description'=>$request->body,
												'author'=>$request->author
												]);
		
		return redirect()->route('home')->with('updatemessage', 'Blog updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Blog $blog)
    {
        //
		$id = $request->blogId;
		$blog = Blog::find($id);
		$blog->delete();
		return redirect()->route('home')->with('deletemessage', 'Blog deleted successfully');
    }
}
