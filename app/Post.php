<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

     public function Category()
    {
        return $this->belongsTo('App\category');
    }


 	public function detail()
    {
        return $this->hasOne('App\Post_detail');
    }

}
