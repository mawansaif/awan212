<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function () {
    Route::get('/','BlogController@index')->name('home');
	Route::post('/', 'BlogController@store');
	Route::post('/update','BlogController@update');
	Route::post('/delete','BlogController@destroy');


	Route::get('/category','CategoryController@index')->name('category');
	Route::post('/category','CategoryController@store');
	Route::post('/category/update','CategoryController@update');
	Route::post('/category/delete','CategoryController@destroy');

	Route::get('/post','PostController@index')->name('post');
	Route::get('/create','PostController@create')->name('post/create');
	Route::post('/post/save','PostController@store');
	Route::get('/post/detail/{post}','PostDetailController@index');
	Route::get('/post/edit/{post}','PostController@edit');
	Route::post('/post/update/{post}','PostController@update');
	Route::post('/post/delete','PostController@destroy');


});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('main');
