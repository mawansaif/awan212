
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>

<footer class="bg-dark text-light mt-2 p-5 text-center">
	<p class="h3">This site is build at Next@Solution.</p>
</footer>